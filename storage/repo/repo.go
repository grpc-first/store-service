package repo

import pb "gitlab.com/grpc-first/store-service/genproto/store"

type StoreStorageI interface {
	// adddress crud
	CreateAddresses(*pb.Addresses) (*pb.Addresses, error)
	GetAddressesById(*pb.Id) (*pb.Address, error)
	UpdateAddress(*pb.Address) error
	DeleteAddressById(*pb.Id) error

	// stores crud
	CreateStore(*pb.Store) (*pb.StoreInfo, error)
	GetStoreById(*pb.Id) (*pb.StoreInfo, error)

	// stores products
	AddProductToStore(*pb.StoresProduct) (pb.StoreProductInfo, int64, error)
	GetStoresProductsByIds(*pb.Ids) (*pb.StoresProductsInfo, error)
	
	// sell
	SellProductFromStore(req pb.SellProductRequest, userBalance float64) (*pb.SellProductResponse, error)
}
