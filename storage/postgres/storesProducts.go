package postgres

import (
	pb "gitlab.com/grpc-first/store-service/genproto/store"
)

func (s *StoreRepo) AddProductToStore(req *pb.StoresProduct) (pb.StoreProductInfo, int64, error) {
	_, err := s.Db.Exec(`insert into stores_products(store_id, product_id, amount, price)
	values($1, $2, $3, $4)`, req.StoreId, req.ProductId, req.Amount, req.Price)
	if err != nil {
		return pb.StoreProductInfo{}, 0, err
	}
	response := pb.StoreProductInfo{}
	response.Store, err = s.GetStoreById(&pb.Id{Id: req.StoreId})
	if err != nil {
		return pb.StoreProductInfo{}, 0, err
	}
	prdoductInfo := &pb.ProductInfo{}
	prdoductInfo.Amount = req.Amount
	prdoductInfo.Price = req.Price
	response.Products = append(response.Products, prdoductInfo)
	return response, req.ProductId, nil
}

func (s *StoreRepo) GetStoresProductsByIds(req *pb.Ids) (*pb.StoresProductsInfo, error) {
	response := &pb.StoresProductsInfo{}
	for _, id := range req.Ids {
		tempRes := pb.StoreProductInfo{}
		rows, err := s.Db.Query(`SELECT 
		product_id, 
		price, 
		amount 
		from stores_products 
		where store_id = $1`, id)
		if err != nil {
			return &pb.StoresProductsInfo{}, err
		}
		defer rows.Close()
		tempRes.Store, err = s.GetStoreById(&pb.Id{Id: id})
		if err != nil {
			return &pb.StoresProductsInfo{}, err
		}
		products := []*pb.ProductInfo{}
		for rows.Next() {
			tempProduct := pb.ProductInfo{}
			rows.Scan(&tempProduct.Id, &tempProduct.Price, &tempProduct.Amount)
			products = append(products, &tempProduct)
		}
		tempRes.Products = products
		response.Infos = append(response.Infos, &tempRes)
	}
	return response, nil
}

func (s *StoreRepo) SellProductFromStore(req pb.SellProductRequest, userBalance float64) (*pb.SellProductResponse, error) {
	response := &pb.SellProductResponse{}
	totalsum := 0.0
	err := s.Db.QueryRow(`select SUM(price * $1) as total from stores_products where 
	amount >= $2 and store_id = $3 and product_id = $4`,
		req.Amount, req.Amount, req.StoreId, req.ProductId).Scan(
		&totalsum,
	)
	if err != nil || totalsum > userBalance {
		return nil, err
	}
	product := &pb.ProductInfo{}
	err = s.Db.QueryRow(`update stores_products SET amount = amount-$1 
	where store_id = $2 and product_id = $3 returning price`, req.Amount, req.StoreId, req.ProductId).Scan(
		&product.Price,
	)
	response.Product = product
	if err != nil {
		return nil, err
	}
	response.TotalPrice = float32(totalsum)
	response.ProducCount = 1
	return response, nil
}
