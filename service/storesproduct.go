package service

import (
	"context"

	ps "gitlab.com/grpc-first/store-service/genproto/payment"
	pbp "gitlab.com/grpc-first/store-service/genproto/product"
	pb "gitlab.com/grpc-first/store-service/genproto/store"

	"gitlab.com/grpc-first/store-service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *StoreService) AddProductToStore(ctx context.Context, req *pb.StoresProduct) (*pb.StoreProductInfo, error) {
	res, p_id, err := s.Storage.Store().AddProductToStore(req)
	if err != nil {
		s.Logger.Error("Error while adding product to store", logger.Any("insert", err))
		return &pb.StoreProductInfo{}, status.Error(codes.Internal, "Please check your data")
	}

	products, err := s.Client.ProductService().GetProductsByIds(ctx, &pbp.GetProductsByIdsRequest{Ids: []int64{p_id}})
	if err != nil {
		return &pb.StoreProductInfo{}, err
	}
	p := products.Products[0]
	tempC := &pb.Category{
		Id:   p.Category.Id,
		Name: p.Category.Name,
	}
	tempT := &pb.Type{
		Id:   p.Type.Id,
		Name: p.Type.Name,
	}
	temp := res.Products[0]
	temp.Id = p.Id
	temp.Category = tempC
	temp.Type = tempT
	temp.Model = p.Model
	temp.Name = p.Name
	res.Products[0] = temp
	return &res, nil
}
func (s *StoreService) GetStoreProductsByIds(ctx context.Context, req *pb.Ids) (*pb.StoresProductsInfo, error) {
	r, err := s.Storage.Store().GetStoresProductsByIds(req)
	if err != nil {
		s.Logger.Error("Error while getting stores products info", logger.Any("Get", err))
		return &pb.StoresProductsInfo{}, status.Error(codes.NotFound, "Not found")
	}
	response := pb.StoresProductsInfo{}
	for _, res := range r.Infos {
		products := []*pb.ProductInfo{}
		for _, p := range res.Products {

			p_infos, err := s.Client.ProductService().GetProductsByIds(ctx, &pbp.GetProductsByIdsRequest{Ids: []int64{p.Id}})
			if err != nil {
				s.Logger.Error("Error while getting products info", logger.Any("Get", err))
				return &pb.StoresProductsInfo{}, status.Error(codes.NotFound, "Not Found")
			}
			re := p_infos.Products[0]
			p.Model = re.Model
			p.Name = re.Name
			t := &pb.Type{
				Id:   re.Type.Id,
				Name: re.Type.Name,
			}
			p.Type = t
			c := &pb.Category{
				Id:   re.Category.Id,
				Name: re.Category.Name,
			}
			p.Category = c
			products = append(products, p)
		}
		res.Products = products
		response.Infos = append(response.Infos, res)
	}

	return &response, nil
}
func (s *StoreService) SellProductFromStore(ctx context.Context, req *pb.SellProductRequest) (*pb.SellProductResponse, error) {
	card, err := s.Client.PaymentService().GetCardInfo(ctx, &ps.Id{OwnerId: req.CustomerId})
	if err != nil {
		s.Logger.Error("error while getting Card Info", logger.Any("Get", err))
		return nil, status.Error(codes.Internal, "do you have credit card")
	}
	response, err := s.Storage.Store().SellProductFromStore(*req, float64(card.Balance))
	if err != nil {
		s.Logger.Error("s.Storage.Store().SellProductFromStore(req, float64(card.Balance))", logger.Any("Get", err))
		return nil, status.Error(codes.Internal, "Check your balance")
	}
	product, err := s.Client.ProductService().GetProductsByIds(ctx, &pbp.GetProductsByIdsRequest{Ids: []int64{req.ProductId}})
	if err != nil {
		s.Logger.Error("s.Client.ProductService().GetProductsByIds(ctx, &pbp.GetProductsByIdsRequest{Ids: []int64{req.ProductId}})", logger.Any("Get", err))
		return nil, status.Error(codes.Internal, "Check your balance")
	}
	p := product.Products[0]
	response.Product = &pb.ProductInfo{
		Id:    p.Id,
		Name:  p.Name + " " + p.Model,
		Model: p.Model,
		Type:  (*pb.Type)(p.Type),
		Category: &pb.Category{
			Id:   p.Category.Id,
			Name: p.Category.Name,
		},
	}
	storeInfo, err := s.Storage.Store().GetStoreById(&pb.Id{Id: req.StoreId})
	if err != nil {
		s.Logger.Error("s.Storage.Store().GetStoreById(&pb.Id{Id: req.StoreId})", logger.Any("Get", err))
		return nil, status.Error(codes.Internal, "Check your balance")
	}
	response.Store = storeInfo

	// payment level
	pay := &ps.UpdateBalanceRequest{
		OwnerId: req.CustomerId,
		Balance: response.TotalPrice,
	}
	_, err = s.Client.PaymentService().SubtractBalance(ctx, pay)
	if err != nil {
		s.Logger.Error("s.Client.PaymentService().SubtractBalance(ctx, pay)", logger.Any("Update", err))
		return nil, status.Error(codes.Internal, "Check your balance")
	}
	response.IsPaid = true
	return response, nil
}
