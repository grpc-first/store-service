package grpcclient

import (
	"fmt"

	"gitlab.com/grpc-first/store-service/config"
	ps "gitlab.com/grpc-first/store-service/genproto/payment"
	pbd "gitlab.com/grpc-first/store-service/genproto/product"
	us "gitlab.com/grpc-first/store-service/genproto/user"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManager struct {
	config         config.Config
	productService pbd.ProductServiceClient
	paymentService ps.PaymentServiceClient
	userService us.UserServiceClient
}

func New(c config.Config) (*ServiceManager, error) {
	connection, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.ProductServiceHost, c.ProductServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial product service: host: %s and port: %d",
			c.ProductServiceHost, c.ProductServicePort)
	}
	paymentconnection, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.PaymentServiceHost, c.PaymentServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial Payment service: host: %s and port: %d",
			c.ProductServiceHost, c.ProductServicePort)
	}
	userconnection, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.UserServiceHost, c.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial User service: host: %s and port: %d",
			c.UserServiceHost, c.UserServicePort)
	}
	serviceManager := &ServiceManager{
		config:         c,
		productService: pbd.NewProductServiceClient(connection),
		paymentService: ps.NewPaymentServiceClient(paymentconnection),
		userService: us.NewUserServiceClient(userconnection),
	}

	return serviceManager, nil
}

func (s *ServiceManager) ProductService() pbd.ProductServiceClient {
	return s.productService
}

func (s *ServiceManager) PaymentService() ps.PaymentServiceClient {
	return s.paymentService
}

func (s *ServiceManager) UserService() us.UserServiceClient {
	return s.userService
}
