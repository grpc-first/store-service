CREATE TABLE IF NOT EXISTS stores_products (
    store_id INTEGER REFERENCES stores(id) ON DELETE CASCADE,
    product_id INTEGER NOT NULL,
    price DECIMAL(9, 2) NOT NULL CHECK(price >= 0),
    amount DECIMAL(9, 2) NOT NULL CHECK(amount >= 0)
);